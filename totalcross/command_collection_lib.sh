#!/bin/bash

command_list=()
command_literal=()
command_description=()

declare -A command_map

declare -i insert_pos=0

add_command() {
	cmd="$1"
	cmd_mnemonic="${2:-$1}"
	
	if [ "$cmd_mnemonic" = - ]; then
		cmd_mnemonic="$cmd"
	fi
	
	cmd_description="${3:-${cmd_mnemonic}}"
	
	command_list+=( ${cmd_mnemonic} )
	command_literal+=( "${cmd}" )
	command_description+=( "${cmd_description}" )
	command_map[${cmd_mnemonic}]=$insert_pos
	insert_pos=${insert_pos}+1
}

list_commands() {
	echo "${command_list[@]}"
}

describe_commands() {
	for cmd in "${command_list[@]}"; do
		describe_command "${cmd}"
	done
}

describe_command() {
	cmd_name="$1"
	idx=${command_map[$cmd_name]}
	cmd_description="${command_description[$idx]}"
	echo -e "\t$cmd_name : $cmd_description"
}

call_command() {
	cmd_name="$1"
	shift
	
	idx=${command_map[$cmd_name]}
	cmd_check=${command_list[$idx]}
	
	if [ "$cmd_name" != "$cmd_check" ]; then
		echo >&2 "Command \"$cmd_name\" does not match a recognized command"
		return 1
	fi
	
	cmd="${command_literal[$idx]}"

	$cmd "$@"
}
