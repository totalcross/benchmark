#!/bin/bash

. command_collection_lib.sh

add_command describe_commands list "Describes availables commands"
add_command help - "Prints help"

extract_bench_info() {
	echo `run_bench "$1" name`:`run_bench "$1" short`
}

run_bench() {
	crude_cmd="$1"
	bench_path="${1%/*}"
	bench="${1##*/}"
	
	shift
	
	(
		if [ "$bench_path" != "$crude_command" ]; then
			cd "${bench_path}"
			bench=./${bench}
		fi
		
		${bench} "$@"
	)
}

search_benchs() {
	for bench in `find -name *.bench`; do
		if [ -f "$bench" ]; then
			bench_desc=`extract_bench_info ${bench}`
			
			bench_name="${bench_desc%%:*}"
			bench_short="${bench_desc#*:}"
			
			add_command "run_bench $bench" "$bench_name" "$bench_short"
		fi
	done
}

help() {
	describe_commands
}

search_benchs

while [ $# -gt 0 ]; do
	case "$1" in
		-h|--help)
			help
			exit
			;;
		*)
			call_command "$@"
			
			break
			;;
	esac
	shift
done
